﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Data;


namespace DataTest
{
    [TestClass]
    public class LoginTest
    {

        [TestMethod]
        public void TestGetUser()
        {
            string username = "test";
            AccountDatabase test = new AccountDatabase();
            var output = test.GetUser(username);
            Assert.AreEqual(output.Username, "test");
        } 
    }
}
