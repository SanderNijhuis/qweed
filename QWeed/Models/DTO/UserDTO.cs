﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.DTO
{
    public class UserDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Motivation { get; set; }
        public DateTime LastLogin { get; set; }
        public int ID { get; set; }
    }
}
