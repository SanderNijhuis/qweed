﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.DTO
{
    public class RegistrationDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Motivation { get; set; }
    }
}
