﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ViewModel
{
    public class AuthView
    {
        public string Name { get; set; }
        public string AuthKey { get; set; }
    }
}
