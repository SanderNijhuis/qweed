using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LogicTest
{
    using Data;

    using Logic;

    [TestClass]
    public class UserlogicTest
    {

        Authentication auth = new Authentication();
        AccountDatabase DB = new AccountDatabase();

        [TestMethod]
        public void AddAccount()
        {
            var user = this.auth.GetUser("Test123");
            auth.RemoveAccount(user.ID);
            auth.AddAccount("Test123", "test123", "For the first time in forever");
            var result = DB.GetUser("Test123");
            Assert.AreEqual(result.Username, "Test123");
        }
    }
}
