﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    using Models.DTO;

    using MySql.Data.MySqlClient;

    public class AccountDatabase : IDatabaseAccount
    {
        private readonly DatabaseConnection _connect;
        public AccountDatabase(DatabaseConnection connect)
        {
            _connect = connect;
        }

        public AccountDatabase()
        {
            _connect = new DatabaseConnection();
        }
        public bool Login(string username, DateTime dateTime, string AuthKey)
        {
            throw new NotImplementedException();
        }

        public void Logout(string AuthKey)
        {
            throw new NotImplementedException();
        }

        public void AddAccount(RegistrationDTO account)
        {
            _connect.Con.Open();
            try
            {
                MySqlCommand cmd = _connect.Con.CreateCommand();
                cmd.CommandText = "INSERT INTO `User` (`Name`, `Password`,`Motivation`) VALUES (@Name, @Password, @Motivation)";
                cmd.Parameters.AddWithValue("@Name", account.Username);
                cmd.Parameters.AddWithValue("@Password", account.Password);
                cmd.Parameters.AddWithValue("@Motivation", account.Motivation);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                this._connect.Con.Dispose();
            }
        }

        public bool RemoveAccount(int pID)
        {
            _connect.Con.Open();
            try
            {
                MySqlCommand cmd = _connect.Con.CreateCommand();
                cmd.CommandText = "DELETE FROM `User` WHERE ID = @ID";
                cmd.Parameters.AddWithValue("@ID", pID);
                cmd.ExecuteNonQuery();
                
            }
            catch (Exception e)
            {
                throw e;
                
            }
            finally
            {
                _connect.Con.Close();
                
            }
            return true;
        }

        public UserDTO GetUser(string pUsername)
        {
            UserDTO account = new UserDTO();
            try
            {
                _connect.Con.Open();

                string query = "SELECT User.ID, User.Password, User.Motivation FROM User WHERE User.Name = @name";
                MySqlCommand cmd = new MySqlCommand(query, _connect.Con);
                cmd.Parameters.AddWithValue("@name", pUsername);
                var dataReader = cmd.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        account.ID = dataReader.GetInt32("ID");
                        account.Username = pUsername;
                        account.Password = dataReader.GetString("Password");
                        account.Motivation = dataReader.GetString("Motivation");
                    }
                }
                dataReader.Close();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _connect.Con.Close();
            }

            return account;
        }
    }
}
