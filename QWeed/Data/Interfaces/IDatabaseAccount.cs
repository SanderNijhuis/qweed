﻿using System;
using Models.DTO;

namespace Data
{
    public interface IDatabaseAccount
    {
        bool Login(string username, DateTime dateTime, string AuthKey);

        void Logout(string AuthKey);

        void AddAccount(RegistrationDTO account);

        bool RemoveAccount(int ID);

        UserDTO GetUser(string username);

        
    }
}
